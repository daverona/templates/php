<?php

use PHPUnit\Framework\TestCase;
use daVerona\Aeon\Hello;

class HelloTest extends TestCase
{
    public function testIsObject()
    {
        $instance = new Hello();
        $this->assertTrue(is_object($instance));
        unset($instance);
    }

    public function testSay()
    {
        $instance = new Hello();
        ob_start();
        $instance->say();
        $capture = ob_get_contents();
        ob_end_clean();
        $this->assertTrue($capture == "\n\nHello, World!\n\n\n");
        unset($instance);
    }
}
