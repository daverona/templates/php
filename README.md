# daverona/php

[![pipeline status](https://gitlab.com/daverona/templates/php/badges/master/pipeline.svg)](https://gitlab.com/daverona/templates/php/-/commits/master)
[![coverage report](https://gitlab.com/daverona/templates/php/badges/master/coverage.svg)](https://gitlab.com/daverona/templates/php/-/commits/master)

This is a PHP package template. 

## Quick Start

For testing, make a directory and go into it to create `composer.json` file with:

```json
{
  "repositories": [
    {
      "type": "vcs",
      "url": "ssh://git@gitlab.com/daverona/templates/php.git"
    }
  ]
}
```

Then run:

```bash
composer require daverona/aeon:dev-master
./vendor/bin/aeon-says
```

If "Hello, World!" message shows up, it's working.

Make `index.php` with:

```php
<?php

require_once __DIR__."/vendor/autoload.php";

use daVerona\Aeon\Hello;

$speaker = new Hello();
$speaker->say();
```

And run:

```bash
php index.php
```

The "Hello, World!" message should show up again.

Since both of the executable script (`aeon-says`) and the package class (`daVerona\Aeon\Hello`) are working, let's uninstall:

```bash
composer remove daveron/aeon
```

## Development 

It's time to make this template yours.
Make your own repository by *importing* not forking this template.
If you don't know what importing means, copy this template to your own repository.
We assume that you have a repository exactly same as this template.

Clone the newly created repository to your local machine if you haven't done so already. 
Let's assume it is cloned to `path/to/package/php`.

You need a new or existing project, which uses your package.
I.e. your project is a *user* of your package.
Go to the project and update `composer.json` file with:

```json
{
  "repositories": [
    {
      "type": "path",
      "url": "path/to/package/php"
    }
  ],
  "require": {
    "daverona/aeon": "*"
  }
}
```

Then install your package to your project:

```bash
composer require daverona/aeon:dev-master
```

`composer.json` and `composer.lock` are updated and `vendor` directory contains `daverona/aeon` 
as a symbolic link to `path/to/package/php`.
The package under `path/to/package/php` is editable.
If the package updated, you should be able to see the changes in your project.
Don't forget to *commit* changes to the package repository.

Once you are comfortable with your own package, review `composer.json` in the package for customization.
Before customize `composer.json`, remove the package from the project:

```bash
composer remove daverona/aeon
```

## Production

Please read [https://gitlab.com/daverona/docs/php/-/blob/master/repo.md](https://gitlab.com/daverona/docs/php/-/blob/master/repo.md).

## References

* Composer: [https://getcomposer.org/doc/](https://getcomposer.org/doc/)
* Creating Composer Package Library: [http://www.darwinbiler.com/creating-composer-package-library/](http://www.darwinbiler.com/creating-composer-package-library/)
